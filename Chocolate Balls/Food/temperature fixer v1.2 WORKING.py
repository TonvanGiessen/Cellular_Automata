# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 12:03:57 2018

@author: SvanS
"""


import csv


li1=[]

with open('Ocean T.csv')as csvfile: #open file with data
    reader=csv.reader(csvfile, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)

    c=0.
    li0=[]
    for row in reader:
        if c == row[0]:
            if 99999.0 not in row:
                li0.append(row[1:])
        else:
            la=[c]
            la.extend([sum(x)/len(x) for x in zip(*li0)])
            li1.append(la)
            li0=[]
            if 99999.0 not in row:
                li0.append(row[1:])
            c+=1.
    la=[c]
    la.extend([sum(x)/len(x) for x in zip(*li0)])
    li1.append(la)

print li1
with open('Ocean p Triangle.csv',"wb") as f:
        writer = csv.writer(f)
        writer.writerows(li1)


