# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 08:53:15 2018

@author: Nils
"""

import csv

GD_file = open('General_Database.csv', 'r')
  #reader = csv.reader(GD)
  #GD_list = list(reader)

GD = csv.reader (GD_file)

LU_abiotic = []
LU_agri = []
LU_green = []
LU_urban = []
LU_water = []
pop = []
rain = []
temp = []

for row in GD:
    LU_abiotic.append(row[2])
    LU_agri.append(row[3])
    LU_green.append(row[4])
    LU_urban.append(row[5])
    LU_water.append(row[6])
    pop.append(row[8])
    rain.append(row[9])
    temp.append(row[10])

print LU_agri

GD_file.close()
