import Rhino.Geometry as rg
import System.Drawing.Color as col

def Voxeldrawing(corner,Plane,U,V,Z):
    VoxelMesh=rg.Mesh()
    N000=corner#0
    N001=N000+Plane.XAxis*U#1
    N010=N000+Plane.YAxis*V#2
    N011=N001+Plane.YAxis*V#3
    N100=N000+Plane.ZAxis*Z#4
    N101=N001+Plane.ZAxis*Z#5
    N110=N010+Plane.ZAxis*Z#6
    N111=N011+Plane.ZAxis*Z#7
    VoxelMesh.Vertices.Add(N000)
    VoxelMesh.Vertices.Add(N001)
    VoxelMesh.Vertices.Add(N010)
    VoxelMesh.Vertices.Add(N011)
    VoxelMesh.Vertices.Add(N100)
    VoxelMesh.Vertices.Add(N101)
    VoxelMesh.Vertices.Add(N110)
    VoxelMesh.Vertices.Add(N111)
    VoxelMesh.Faces.AddFace(0,2,3,1)
    VoxelMesh.Faces.AddFace(0,4,6,2)
    VoxelMesh.Faces.AddFace(4,5,7,6)
    VoxelMesh.Faces.AddFace(1,3,7,5)
    VoxelMesh.Faces.AddFace(2,6,7,3)
    VoxelMesh.Faces.AddFace(0,1,5,4)
    return VoxelMesh



def Combinevoxels(Pl,m,n,p,Usize,Vsize,Zsize):
    voxels=[]
    for i in range(0,m):
        for j in range(0,n):
            for k in range(0,p):
                corner=Pl.Origin+Pl.XAxis*i*Usize+Pl.YAxis*j*Vsize+Pl.ZAxis*k*Zsize
                voxel=Voxeldrawing(corner,Pl,Usize,Vsize,Zsize)
                voxels.Add(voxel)
    return voxels




M=Combinevoxels(Pl,m,n,p,Usize,Vsize,Zsize)