  private void RunScript(Mesh M, ref object A)
  {
    if(M.Faces.TriangleCount != M.Faces.Count){throw new Exception("the input mesh must be triangular!");}
    Mesh SubdividedMesh = new Mesh();

    int FC = M.Faces.Count;
    for(int k = 0; k < FC;k++){
      Point3f v0f;
      Point3f v1f;
      Point3f v2f;
      Point3f v3f;
      M.Faces.GetFaceVertices(k, out v0f, out v1f, out v2f, out v3f);

      Point3d v0 = new Point3d(v0f);
      Point3d v1 = new Point3d(v1f);
      Point3d v2 = new Point3d(v2f);

      Point3d v01 = 0.5 * (v0 + v1);
      Point3d v12 = 0.5 * (v1 + v2);
      Point3d v20 = 0.5 * (v2 + v0);

      Mesh t0 = new Mesh();
      Mesh t1 = new Mesh();
      Mesh t2 = new Mesh();
      Mesh t3 = new Mesh();

      t0.Vertices.AddVertices(new Point3d[]{v0,v01,v20});
      t1.Vertices.AddVertices(new Point3d[]{v1,v12,v01});
      t2.Vertices.AddVertices(new Point3d[]{v2,v20,v12});
      t3.Vertices.AddVertices(new Point3d[]{v01,v12,v20});
      t0.Faces.AddFace(0, 1, 2);
      t1.Faces.AddFace(0, 1, 2);
      t2.Faces.AddFace(0, 1, 2);
      t3.Faces.AddFace(0, 1, 2);
      SubdividedMesh.Append(t0);
      SubdividedMesh.Append(t1);
      SubdividedMesh.Append(t2);
      SubdividedMesh.Append(t3);
    }
    A = SubdividedMesh;
  }